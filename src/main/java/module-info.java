module eu.ngong.sampleFxml {
	requires transitive javafx.controls;
	requires javafx.fxml;

	opens eu.ngong.sampleFxml to javafx.fxml;

	exports eu.ngong.sampleFxml;
}
