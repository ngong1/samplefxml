package eu.ngong.sampleFxml;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;

public class PrimaryController implements Initializable {

	String[] values = { "one", "two", "three", "four", "five" };

	@FXML
	private ListView<String> theList;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		this.theList.getItems().addAll(values);

	};

}
